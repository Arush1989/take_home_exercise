AUTOMATION EXERCISE:

The automation project is located in the 'Automation Exercise' folder

-	This test project is based on Selenium WebDriver (Page Object Model) using Java as the programming language.
-	This is a Maven based project which used TestNG as the test framework.
-	The tests have been configured to run on Chrome browser on Windows machine.
-	The test and pages are present in the ‘src’ folder in pages and tests locations respectively.
-	The test reports (html and xml) are generated at the following location inside the project: test-output/confluenceTestSuite


Pre-requisites for running automation tests:

1. Install Java
2. Install Maven
3. Install TestNG in Eclipse or IDE used for Java development
4. Download chromedriver and place it in the following locaiton "C:/Temp"


Scenarios covered:

Tests have been written to test if the view and edit restrictions can be changed by a user. Once the restrcitions change, the same has been validated for a non logged in user.



IMPORTANT:

The config file needs to be updated with Confluence Page link, email and password. I have removed my username and password from file due to security reason and my account might expire soon.
the config file is placed at the following location src/config/config.properties
Please update with any page that needs to be tested and user credentials


Steps to execute tests:
1. To execute tests from Ecliplse/any Java IDE, right click on testng.xml file and run as Test NG suite
2. To execute tests using command line, browse to the project folder and run the following command: 'mvn clean install test'