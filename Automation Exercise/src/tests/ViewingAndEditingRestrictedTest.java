package tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import pages.ConfluencePage;
import pages.LoginPage;

public class ViewingAndEditingRestrictedTest extends TestBase {

	ConfluencePage testPage;
	LoginPage loginPage;

	@BeforeClass
	public void beforeTest() {
		initialize();
		testPage = new ConfluencePage();
		testPage.clickSignInButton();
		loginPage = new LoginPage();
		loginPage.login(prop.getProperty("email"), prop.getProperty("password"));
	}

	@Test
	public void verifyUserCanSelectViewingAndEditingRestricted() throws InterruptedException {
		testPage.clickLockIcon();
		testPage.selectViewingAndEditingRestrictedFromDropdown();
		Thread.sleep(2000);
		testPage.logout();
		openConfluencePage();
		Assert.assertFalse(testPage.isPageTitlePresent());
	}

	@AfterClass
	/* Method to restore no restrictions on page */
	public void restoreDefaultRestrcitions() throws InterruptedException {
		loginPage = new LoginPage();
		testPage = new ConfluencePage();
		loginPage.login(prop.getProperty("email"), prop.getProperty("password"));
		testPage.clickLockIcon();
		testPage.selectNoRestrictionsFromDropdown();
		driver.quit();
	}

}
