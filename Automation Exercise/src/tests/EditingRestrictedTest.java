package tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import pages.ConfluencePage;
import pages.LoginPage;

public class EditingRestrictedTest extends TestBase {

	ConfluencePage testPage;
	LoginPage loginPage;

	@BeforeClass
	public void beforeTest() {
		initialize();
		testPage = new ConfluencePage();
		testPage.clickSignInButton();
		loginPage = new LoginPage();
		loginPage.login(prop.getProperty("email"), prop.getProperty("password"));
	}

	@Test
	public void verifyUserCanSelectEditingRestricted() throws InterruptedException {
		testPage.clickLockIcon();
		testPage.selectEditingRestrictedFromDropdown();
		Thread.sleep(2000);
		testPage.logout();
		openConfluencePage();
		Assert.assertFalse(testPage.isEditButtonPresent());
	}


	@AfterClass
	public void tearDown() throws InterruptedException {
		driver.quit();
	}

}
