package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;

public class ConfluencePage extends TestBase {

	// Capturing web elements
	@FindBy(css = "[aria-label='Sign In']")
	WebElement signInButton;

	@FindBy(css = "[href='/wiki/logout.action']")
	WebElement logoutButton;

	@FindBy(id = "title-text")
	WebElement pageTitle;

	@FindBy(css = "[data-test-id='restrictions.dialog.button']")
	WebElement lockIcon;

	@FindBy(css = ".css-1c4m5o5")
	WebElement restrictionsDialog;

	@FindBy(css = ".css-vxcmzt > div:nth-child(1)")
	WebElement applyButton;

	@FindBy(css = ".css-vxcmzt > div:nth-child(1)")
	WebElement cancelButton;

	@FindBy(css = "[data-test-id='restrictions-dialog.content-mode-select']")
	WebElement restrictionsDropdown;

	@FindBy(xpath = "//*[@class=\" css-1ljkvdv\"]/div")
	WebElement noRestrictrions;

	@FindBy(xpath = "//*[@class=\" css-1ljkvdv\"]/div[2]")
	WebElement editingRestricted;

	@FindBy(xpath = "//*[@class=\" css-1ljkvdv\"]/div[3]")
	WebElement viewingAndEditingRestricted;

	@FindBy(id = "editPageLink")
	WebElement editButton;

	// Initializing the web elements
	public ConfluencePage() {
		PageFactory.initElements(driver, this);
	}

	// Performing actions on the captured web elements
	public void clickSignInButton() {
		signInButton.click();
	}

	public void clickLockIcon() {
		lockIcon.click();
	}

	// Method to select Editing Restricted option from Restrictions dialog
	public void selectEditingRestrictedFromDropdown() {
		Actions action = new Actions(driver);
		action.moveToElement(restrictionsDropdown);
		action.click().build().perform();
		editingRestricted.click();
		applyButton.click();
	}

	// Method to select No Restrictions option from Restrictions dialog
	public void selectNoRestrictionsFromDropdown() {
		Actions action = new Actions(driver);
		action.moveToElement(restrictionsDropdown);
		action.click().build().perform();
		noRestrictrions.click();
		applyButton.click();
	}

	// Method to select Viewing and Editing Restricted option from Restrictions
	// dialog
	public void selectViewingAndEditingRestrictedFromDropdown() {
		Actions action = new Actions(driver);
		action.moveToElement(restrictionsDropdown);
		action.click().build().perform();
		viewingAndEditingRestricted.click();
		applyButton.click();
	}

	public void clickApplyButton() {
		applyButton.click();
	}

	public void clickCancelButton() {
		cancelButton.click();
	}

	// method to check if Page title is present on page
	public boolean isPageTitlePresent() {
		try {
			pageTitle.isDisplayed();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	// method to check if edit button is present on page
	public boolean isEditButtonPresent() {
		try {
			editButton.isDisplayed();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	// method to logout from Atlassian
	public void logout() throws InterruptedException {
		driver.findElement(By.xpath(
				"//*[@id=\"confluence-ui\"]/div[3]/div/div[1]/div/header/div/div[3]/span/div/button/span/span/div/span/span"))
				.click();
		logoutButton.click();
		Thread.sleep(3000);
	}

}
