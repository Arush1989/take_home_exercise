package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;

public class LoginPage extends TestBase{
	
	//Capturing login page elements
	@FindBy(id = "username")
	WebElement email;
	
	@FindBy(id="login-submit")
	WebElement submitButton;
	
	@FindBy(id="password")
	WebElement password;
	
	//initializing login page elements
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	//Actions
	public void login(String un, String pass) {
		email.sendKeys(un);
		submitButton.click();
		password.sendKeys(pass);
		submitButton.click();
	}

}
